<?php

require __DIR__.'/lib/Ship.php';

class Ship
{
    public function printShipSummary($someShip) {

        echo 'Ship Name: '.$someShip->getName();
        echo '<hr/>';
        echo $someShip->getNameAndSpecs(false);
        echo '<hr/>';
        echo $someShip->getNameAndSpecs(true);
    }
}

$myShip = new Ship();
$myShip->name = 'Jedi';
$myShip->weaponPower = 10;
$myShip->printShipSummary($myShip);
echo '<br>'; echo '<br/>';

$otherShip = new Ship();
$otherShip->name = 'Imperial Shuttle';
$otherShip->weaponPower = 5;
$otherShip->strength = 50;
$otherShip->printShipSummary($otherShip);
echo '<br>'; echo '<br/>';

if ($myShip->doesGivenShipHaveMoreStrength($otherShip)) {
    echo $otherShip->name . ' has more strength';
} else {
    echo $myShip->name . ' has more strength';
}
